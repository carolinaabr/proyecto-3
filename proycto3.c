/* SEBASTIAN SEREY - 19.542.748-8 - PARALOLO 1 / CAROLINA ECHIBURU - 20.171.816-3  - PARALELO 1 */

// Aqui comienza el codiogo base en el cual se implementa el menu principal para los usuarios y 3 funciones principales.

#include <stdio.h>

void nombre_carrera(){
	return 0;

}

void carrera_codigo(){

	return 0;
}

//Esta funcion sirve para que el usuario pueda consultar el la ponderacion por carrera dentro de la universidad de valparaiso. 
void consultar_ponderacion_carrera()
{      
	int opcion2;
	do{
        printf( "\n   1. Elija nombre carrera");
        printf( "\n   2. Elija carrera por codigo");
        printf( "\n   3. Salir.");
        printf( "\n\n   Elija alguna opcion opción : ");
        scanf( "%d", &opcion2);
        switch (opcion2){
            case 1: carrera_nombre(); 
                    break;

            case 2:  carrera_codigo(); 
                    break;
         }

    } while (opcion2 != 3);
}

//Esta funcion sirve para que el usuario pueda simular sus puntajes y ver su ponderacion por carrera dentro de la universidad de valparaiso.
void simular_postulacion_carrera()
{
	int nem=0, ranking=0, lenguaje=0, matematicas=0, historia=0, ciencias=0, historia=0, ciencias=0,salir ;


  do{
  printf("¿Usted dio la prueba de Historia y Ciencias Sociales?:\n");
  printf("Si la dio presione 1\n");
  printf("Si no la dio presione 2\n")
  scanf("%i",&historia);
  printf("¿Usted dio la prueba de Ciencias?:\n");
  printf("Si la dio presione 1\n");
  printf("Si no la dio presione 2\n")
  scanf("%i",&ciencias);

  printf("Siguiente paso\n");
  printf("complete con tus distintos Puntajes:\n");
  printf("Ingrese su puntaje NEM:\n");
  scanf("%i", &nem);
  printf("Ingrese su puntaje Ranking:\n");
  scanf("%i", &ranking);
  printf("Ingrse su puntaje de Lenguaje y Comunicación\n");
  scanf("%i", &lenguaje);
  printf("Ingrese su puntaje Matematicas:\n");
  scanf("%i", &matematicas);

  if (historia == 1){
    printf("Ingrese su puntaje de Historia y Ciencias Sociales\n");
    scanf("%i", &historia);
  }
  if (ciencias == 1){
    printf("Ingrese su puntaje de Ciencias\n");
    scanf("%i", &ciencias);
  }else{
    printf("No cumple con los requisitos de postulacion para entrar a la UV\n");
  }

  printf("--------------------------------------------------\n");
  printf("--------------------------------------------------\n");
  printf("Su puntaje NEM: %i\n", nem);
  printf("Su puntaje Ranking es: %i\n", ranking);
  printf("Su puntaje Lenguaje es: %i\n", lenguaje);
  printf("Su puntaje Matematicas es: %i\n", matematicas);
  if (historia == 1){
    printf("Su puntaje Historia es: %i\n", historia);
  }
  if (ciencias == 1){
    printf("Su puntaje Ciencias: %i\n", ciencias);
  }
  printf("-------------------------------------------------------\n");

  printf("si salir presione: 4 \n")
  scanf("%d",&salir)

  }while(salir != 4);
}

 //Se da a elegir un menu para seleccionar facultades, y entregar los datos de las diferentes carreras presentes en estas.
void mostrar_ponderacion_facultad()
{
    int opciones;
     do{
     	printf("¿Que facultad desea ver?\n");
        printf( "\n   1. Facultad de Arquitectura");
        printf( "\n   2. Facultad de Ciencias");
        printf( "\n   3. Facultad de ciencias del mar y de recursos naturales");
        printf( "\n   4. Facultad de ciencias economicas y administrativas");
        printf( "\n   5. Facultad de derechos y ciencias Sociales");
        printf( "\n   6. Facultad de farmacia");
        printf( "\n   7. Facultad de humanidades");
        printf( "\n   8. Facultad de ingeniaria");
        printf( "\n   9. Facultad de medicina");
        printf( "\n   10. Facultad de odontologia");
        printf( "\n   11. Salir.");
        printf( "\n\n   Elija alguna opcion opción : ");
        scanf( "%d", &opciones);
        switch (opciones){
            case 1: facultad1(); 
                    break;

            case 2:  facultad2(); 
                    break;

            case 3: facultad3(); 
                    break;

            case 4: facultad4(); 
                    break;

            case 5:  facultad5(); 
                    break;

            case 6: facultad6(); 
                    break;

            case 4: facultad7(); 
                    break;

            case 5:  facultad8(); 
                    break;

            case 6: facultad9(); 
                    break;

            case 4: facultad10(); 
                    break;

         }

    } while (opcion != 11);
}

void facultad1(){
	FILE *file;
	int i = 0;
	if((file = fopen("facultad1.txt","r")) == NULL)
	{
		printf("\nerror al abrir el archivo");
		exit(0); 
	}
	else
	{
		printf("\nLas carreras son: \n");
		while (feof(file) == 0) 
		{
			fscanf(file,"%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			printf("%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			i++;
		}
		fclose(file);
	} 

}

void facultad2(){
FILE *file;
	int i = 0;
	if((file = fopen("facultad2.txt","r")) == NULL)
	{
		printf("\nerror al abrir el archivo");
		exit(0); 
	}
	else
	{
		printf("\nLas carreras son: \n");
		while (feof(file) == 0) 
		{
			fscanf(file,"%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			printf("%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			i++;
		}
		fclose(file);
	} 
}
void facultad3(){
FILE *file;
	int i = 0;
	if((file = fopen("facultad3.txt","r")) == NULL)
	{
		printf("\nerror al abrir el archivo");
		exit(0); 
	}
	else
	{
		printf("\nLas carreras son: \n");
		while (feof(file) == 0) 
		{
			fscanf(file,"%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			printf("%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			i++;
		}
		fclose(file);
	} 
}
void facultad4(){
	FILE *file;
	int i = 0;
	if((file = fopen("facultad4.txt","r")) == NULL)
	{
		printf("\nerror al abrir el archivo");
		exit(0); 
	}
	else
	{
		printf("\nLas carreras son: \n");
		while (feof(file) == 0) 
		{
			fscanf(file,"%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			printf("%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			i++;
		}
		fclose(file);
	} 

}
void facultad5(){
	FILE *file;
	int i = 0;
	if((file = fopen("facultad5.txt","r")) == NULL)
	{
		printf("\nerror al abrir el archivo");
		exit(0); 
	}
	else
	{
		printf("\nLas carreras son: \n");
		while (feof(file) == 0) 
		{
			fscanf(file,"%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			printf("%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			i++;
		}
		fclose(file);
	} 

}
void facultad6(){
	FILE *file;
	int i = 0;
	if((file = fopen("facultad6.txt","r")) == NULL)
	{
		printf("\nerror al abrir el archivo");
		exit(0); 
	}
	else
	{
		printf("\nLas carreras son: \n");
		while (feof(file) == 0) 
		{
			fscanf(file,"%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			printf("%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			i++;
		}
		fclose(file);
	} 

}
void facultad7(){
	FILE *file;
	int i = 0;
	if((file = fopen("facultad7.txt","r")) == NULL)
	{
		printf("\nerror al abrir el archivo");
		exit(0); 
	}
	else
	{
		printf("\nLas carreras son: \n");
		while (feof(file) == 0) 
		{
			fscanf(file,"%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			printf("%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			i++;
		}
		fclose(file);
	} 

}
void facultad8(){
	FILE *file;
	int i = 0;
	if((file = fopen("facultad8.txt","r")) == NULL)
	{
		printf("\nerror al abrir el archivo");
		exit(0); 
	}
	else
	{
		printf("\nLas carreras son: \n");
		while (feof(file) == 0) 
		{
			fscanf(file,"%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			printf("%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			i++;
		}
		fclose(file);
	} 

}
void facultad9(){
	FILE *file;
	int i = 0;
	if((file = fopen("facultad9.txt","r")) == NULL)
	{
		printf("\nerror al abrir el archivo");
		exit(0); 
	}
	else
	{
		printf("\nLas carreras son: \n");
		while (feof(file) == 0) 
		{
			fscanf(file,"%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			printf("%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			i++;
		}
		fclose(file);
	} 

}
void facultad10
FILE *file;
	int i = 0;
	if((file = fopen("facultad10.txt","r")) == NULL)
	{
		printf("\nerror al abrir el archivo");
		exit(0); 
	}
	else
	{
		printf("\nLas carreras son: \n");
		while (feof(file) == 0) 
		{
			fscanf(file,"%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			printf("%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			i++;
		}
		fclose(file);
	} 







void menu() {       // Se crea el menu  principal , que engloban las 3 funciones principales
    int opcion;
//Se importan los datos
    FILE *file;
	int i = 0;
	if((file = fopen("ponderados.txt","r")) == NULL)
	{
		printf("\nerror al abrir el archivo");
		exit(0); 
	}
	else
	{
		printf("\nLas carreras son: \n");
		while (feof(file) == 0) 
		{
			fscanf(file,"%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			printf("%s %d %d %d %d %d %d %d %d %d %f %f %d %d", carreras[i].nombre, carreras[i].codigo, &carreras[i].nem ,&carreras[i].rank, &carreras[i].leng, &carreras[i].mat, &carreras[i].hist, &carreras[i].cs, &carreras[i].pond, &carreras[i].psu, &carreras[i].max, &carreras[i].min, &carreras[i].cuposPsu, &carreras[i].cuposBea);
			i++;
		}
		fclose(file);
	} 
    do{
        printf( "\n   1. Consultar ponderacion carrera");
        printf( "\n   2. Simular postulacion carrera");
        printf( "\n   3. Mostrar ponderaciones facultad");
        printf( "\n   4. Salir.");
        printf( "\n\n   Elija alguna opcion opción : ");
        scanf( "%d", &opcion);
        switch (opcion){
            case 1: consultar_ponderacion_carrera(); 
                    break;

            case 2:  simular_postulacion_carrera(); 
                    break;

            case 3: mostrar_ponderacion_facultad(); 
                    break;
         }

    } while (opcion != 4);
}
int main(){
  
  menu();
  return 0;
}