carrera	facultad	codigo	nem	rank	leng	mar	hist	cs	pond	psu1	max	min	psu	bea

ARQUITECTURA	Arquitectura	19020	10	40	20	20	10	-	500	475	728.80	569.90	120	5

CINE	Arquitectura	19024	10	40	30	10	10	10	500	475	762.40	572.90	40	5

DISENO	Arquitectura	19022	10	40	20	20	10	10	500	475	692.10	504.60	75	4

GESTION EN TURISMO Y CULTURA	Arquitectura	19027	10	40	30	10	10	10	500	475	721.70	549.10	40	5

TEATRO	Arquitectura	19028	10	20	40	10	20	-	500	475	762.76	574.92	20	5

ingenieria en estadisticas	ciencias	19072	10	40	10	30	10	10	500	475	671.80	506.80	30	5

lic. en Ciencias mencion: Biologia o Quimica	ciencias	19075	10	40	20	20	-	10	500	475	735.50	538.10	25	2

lic. en Fisica mencion: Astronomia, Atmosfericas o computacion cientifica	ciencias	19078	10	20	15	45	-	10	500	500	659.65	510.00	45	5

Licenciatura en Matematicas	ciencias	19070	10	40	10	30	10	10	500	475	632.20	506.10	15	2

biologia marina	facultad de ciencias del mar y de recursos naturales	19080	10	40	15	25	-	19	500	475	691.35	502.45	50	5

administracion hotelera y gastronomica	ciencias economicas y administrativas	19086	10	40	20	20	10	10	500	475	731.60	500.80	60	5

administracion publica-valparaiso	ciencias economicas y administrativas	19068	10	20	35	25	10	-	500	500	723.45	574.90	75	2

administracion publica-Santiago	ciencias economicas y administrativas	19005	10	20	35	25	10	-	500	500	655.60	571.45	75	2

auditoria-Valparaiso	ciencias economicas y administrativas	19062	10	40	20	15	15	15	500	475	725.45	509.65	100	5

auditoria-Santiago	ciencias economicas y administrativas	19093	10	40	20	15	15	15	500	475	729.65	500.55	60	5

auditoria vespertino-valparaiso	ciencias economicas y administrativas	19069	10	40	20	15	15	15	500	475	700.15	504.55	20	0

ingenieria comercial -Vina del Mar	ciencias economicas y administrativas	19060	10	40	10	30	10	-	500	500	705.20	578.10	150	12

ingenieria comercial-Santiago	ciencias economicas y administrativas	19095	10	40	10	30	10	-	500	500	647.30	500.90	125	12

ingenieria en informacion y control de negocios	ciencias economicas y administrativas	19083	10	40	10	30	10	10	500	475	722.80	501.80	50	5

ingenieria en Negocios Internacionales-Vina	ciencias economicas y administrativas	19088	10	30	20	30	10	10	500	475	693.00	529.20	80	5

ingenieria en Negocios Internacionales-Stgo	ciencias economicas y administrativas	19089	10	30	20	30	10	10	500	475	694.70	511.80	35	5

derecho	Derecho y Ciencias sociales	19030	10	25	20	20	25	-	500	500	737.30	628.90	135	5

trabajo social	Derecho y Ciencias Sociales	19031	20	30	20	20	10	-	500	475	700.00	556.40	78	2

quimica y farmacia	Farmacia	19010	10	40	20	20	-	10	500	500	760.80	620.50	60	2

nutricion y dietetica	Farmacia	19091	10	40	20	20	-	10	500	500	770.40	608.90	60	2

pedagogia en filosofia	humanidades	19010	10	40	20	10	20	-	500	500	718.90	505.70	30	2

pedagogia en historia y ciencias sociales	humanidades	19011	10	40	20	10	20	-	500	500	741.90	552.60	70	3

pedagogia en musica	humanidades	19015	10	40	25	15	15	-	500	500	619.60	529.95	40	2

sociologia	humanidades	19012	10	40	20	15	15	-	500	475	736.35	546.05	75	2

ingenieria ambiental	ingenieria	19074	10	30	10	40	-	10	500	500	740.70	547.70	55	4

ingenieria civil	ingenieria	19021	10	20	15	45	10	10	500	500	659.10	523.05	90	3

ingenieria civil ambiental	ingenieria	19018	10	20	10	45	-	15	500	500	carrera nueva	carrera nueva	35	2

ingenieria civil biomedica	ingenieria	19076	10	30	10	40	10	10	500	500	749.80	584.00	60	2

ingenieria civil industrial-valparaiso	ingenieria	19052	10	30	10	40	10	10	500	500	690.70	582.20	110	10

ingenieria civil industrial-Santiago	ingenieria	19094	10	30	10	40	10	10	500	500	691.00	563.10	90	10

ingenieria civil informatica	ingenieria	19085	10	30	10	40	10	10	500	500	651.70	541.60	75	5

ingenieria civil matematica	ingenieria	19019	10	20	10	45	-	15	500	500	carrera nueva	15	psu	2

ingenieria civil oceanica	ingenieria	19081	10	40	10	30	10	10	500	475	626.60	501.20	30	2

ingenieria en construccion	ingenieria	19026	10	25	10	45	10	10	500	475	674.55	544.10	80	5

educacion parvularia	Medicina	19037	10	40	30	10	10	10	500	500	652.10	530.20	35	2

enfermeria-Renaca	Medicina	19041	10	40	25	15	-	10	500	500	773.10	681.40	75	2

enfermeria-San Felipe	Medicina	19042	10	40	25	15	-	10	500	500	729.85	645.80	40	2

Fonoaudiologia-Renaca	Medicina	19046	10	40	30	10	-	10	500	500	729.70	633.70	45	2

Fonoaudiologia-San Felipe	Medicina	19044	10	40	30	10	-	10	500	500	664.60	566.10	40	2

Kinesiologia	Medicina	19043	10	30	20	20	-	20	500	500	745.95	602.60	80	2

Medicina-Renaca	Medicina	19040	10	30	20	20	-	20	600	600	815.90	762.50	72	2

Medicina-SanFelipe	Medicina	19039	10	30	20	20	-	20	600	600	764.10	751.30	42	2

Obstetricia y Puericultira-Renaca	Medicina	19047	10	40	30	10	-	10	500	500	778.10	691.10	75	2

Obstetricia y puericultura-San Felipe	Medicina	19036	10	40	30	10	-	10	500	500	723.50	670.20	25	2

Psicologia	Medicina	19045	10	30	30	20	10	-	500	500	700.20	623.80	80	3

Tecnologia Medica-Renaca	Medicina	19040	10	40	25	15	-	10	500	500	756.00	697.20	55	2

tecnologia Medica-San Felipe	Medicina	19048	10	40	25	15	-	10	500	500	819.15	649.10	40	2

Odontologia	Odontologia	19050	10	40	20	20	-	10	500	500	762.10	690.50	72	2









